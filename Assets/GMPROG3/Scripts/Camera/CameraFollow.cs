﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public Transform target;
	public Vector3 offset;
    public float smoothTime = 0.1F;
    private Vector3 velocity = Vector3.zero;

    void Start()
    {
        offset = transform.position - target.transform.position;
    }
	void Update()
	{
		if (target != null) 
		{
            transform.position = Vector3.SmoothDamp(transform.position, target.transform.position + offset, ref velocity, smoothTime);
        }
    }

	}
	
