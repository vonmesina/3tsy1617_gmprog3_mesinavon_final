﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CameraAnimScript : MonoBehaviour {


	public GameObject PlayerCanvas;
	public GameObject MonsterCanvas;
	public GameObject NPCCanvas;
	public GameObject PlayerPos;
	public GameObject MonsterPos;
	public GameObject NPCPos;
	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
	
	}


	public void PlayerCam()
	{
		Camera.main.transform.position = PlayerPos.transform.position;
		PlayerCanvas.SetActive (true);
		MonsterCanvas.SetActive (false);
		NPCCanvas.SetActive (false);
	}

	public void MonsterCam()
	{
		Camera.main.transform.position = MonsterPos.transform.position;
		PlayerCanvas.SetActive (false);
		MonsterCanvas.SetActive (true);
		NPCCanvas.SetActive (false);		
	}

	public void NPCCam()
	{
		Camera.main.transform.position = NPCPos.transform.position;
		PlayerCanvas.SetActive (false);
		MonsterCanvas.SetActive (false);
		NPCCanvas.SetActive (true);
	}

	public void MainMenu()
	{
		Application.LoadLevel (0);
	}

}
