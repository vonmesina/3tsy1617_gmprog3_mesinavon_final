﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefensePotionScript : ItemScript {

	public InventoryScript ItemTaken;
	public DefensePotionScript Potion;
	public int DefValue = 5;
	public PlayerStatsScript Player;

	void Awake()
	{
		ItemTaken = GameObject.FindObjectOfType<InventoryScript>();
		Player = GameObject.FindObjectOfType<PlayerStatsScript>();
	}

	void Update()
	{
		if (ItemConsumed == true)
		{
			DefPotionActivate();
		}
	}


	void OnTriggerEnter(Collider Player)
	{
		if (Player.gameObject.tag == "Player")
		{
            ItemPickedUp();
		}
	}

	public void DefPotionActivate()
	{
		Debug.Log("Health Healed");
		Player.Vitality += DefValue;
	}

	public override void ActivateItem()
	{
		base.ActivateItem();
		DefPotionActivate();
	}

    public void ItemPickedUp()
    {
        ItemTaken.ItemStorage.Add(Potion);
        gameObject.SetActive(false);
    }

}
