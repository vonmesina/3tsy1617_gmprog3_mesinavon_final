﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPotionScript : ItemScript
{

    public InventoryScript ItemTaken;
    public HealthPotionScript Potion;
    public int HealthValue = 30;
    public PlayerStatsScript Player;

    void Awake()
    {
        ItemTaken = GameObject.FindObjectOfType<InventoryScript>();
        Player = GameObject.FindObjectOfType<PlayerStatsScript>();
    }

    void Update()
    {
        if (ItemConsumed == true)
        {
            HealthPotionActivate();
        }
    }


    void OnTriggerEnter(Collider Player)
    {
        if (Player.gameObject.tag == "Player")
        {
            ItemPickedUp();
        }
    }

    public void HealthPotionActivate()
    {
        Debug.Log("Health Healed");
        Player.Health += HealthValue;
    }

    public override void ActivateItem()
    {
        base.ActivateItem();
        HealthPotionActivate();
    }

    public void ItemPickedUp()
    {
        ItemTaken.ItemStorage.Add(Potion);
        gameObject.SetActive(false);
    }

}
