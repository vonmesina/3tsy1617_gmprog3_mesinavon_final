﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InventoryPanelScript : MonoBehaviour 
{

	public InventoryScript Items;
	public ItemScript CurrentItem = null;
	public Text
		Name,
		Info;
	public Image 
		Item,
		ItemNull;

	public List<Button> ButtonList = new List<Button>();
	public List<Image> ImageList = new List<Image>();

	void Start()
	{
		ButtonList.Capacity = 12;
		ImageList.Capacity = 12;

		Name.text = "Select an Item";
		Info.text = " ";

	}

	void Update()
	{
        RefreshInventory();
    }

    float refreshTimer = 0;
    void RefreshInventory()
    {
        if(gameObject.activeSelf)
        {
            refreshTimer += Time.deltaTime;
            if (refreshTimer >= 1)
            {
                for (int value = 0; value < ImageList.Count; value++)
                {
                    ImageList[value].sprite = ItemNull.sprite;
                }

                for (int value = 0; value < Items.ItemStorage.Count; value++)
                {
                    ImageList[value].sprite = Items.ItemStorage[value].Icon.sprite;
                }
                refreshTimer = 0;
            }
        }
       
    }

	//Activate's Inventory UI
	public void ToggleActive()
	{
		if (gameObject.activeSelf) 
		{
			gameObject.SetActive (false);

			for(int value = 0; value < ImageList.Count; value++)
			{
				ImageList [value].sprite = ItemNull.sprite;
			}
		} 
		else if (!gameObject.activeSelf) 
		{
			gameObject.SetActive (true);
			Debug.Log (Items.TotalGold);
			//Debug.Log ("Inventory Activated");
			Debug.Log (Items.ItemStorage.Count);
			for(int value = 0; value <Items.ItemStorage.Count; value++)
			{
				ImageList [value].sprite = Items.ItemStorage [value].Icon.sprite;
			}
		}
	}

	//Updates Data
	public void UpdateData(int DataNumber)
	{
		if(Items.ItemStorage.Count > DataNumber)
		{
		Name.text = Items.ItemStorage [DataNumber].Name.ToString ();
		Info.text = Items.ItemStorage [DataNumber].Info.ToString ();
		}
	}

	//Reverts to original state
	public void DeUpdateData()
	{
		Name.text = "Select an Item";
		Info.text = " ";
	}

	// Uses the Item
	public void UseItem(int IndexNo)
	{
		if(Items.ItemStorage.Count > IndexNo)
		{
			Debug.Log ("Item Used");
			ImageList [IndexNo].sprite = ItemNull.sprite;
			CurrentItem = Items.ItemStorage [IndexNo];
			Items.UseItem (CurrentItem);

		}
	}

}
