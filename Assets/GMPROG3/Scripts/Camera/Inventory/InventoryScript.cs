﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryScript : MonoBehaviour 
{
	public List<ItemScript> ItemStorage = new List<ItemScript>();

	public int TotalGold;

	void Start()
	{
		ItemStorage.Capacity = 12;
	}

	public void UseItem(ItemScript X)
	{
		X.ActivateItem ();
		ItemStorage.Remove (X);
		Destroy (X);
	}
}
