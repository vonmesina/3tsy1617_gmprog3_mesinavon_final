﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemScript : MonoBehaviour 
{

	public string Name = "NullItem";
	public string Info = "NullInfo";
	public int Value = 100;
	public Image Icon;
    public bool ItemConsumed = false;


	public virtual void ActivateItem()
	{
        ItemConsumed = true;
        Debug.Log("Item Destroyed");
		Destroy (gameObject);
	}
}
