﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPotionScript : ItemScript 
{

	public InventoryScript ItemTaken;
	public AttackPotionScript Potion;
	public int AttValue = 10;
	public PlayerStatsScript Player;

	void Awake()
	{
		ItemTaken = GameObject.FindObjectOfType<InventoryScript>();
		Player = GameObject.FindObjectOfType<PlayerStatsScript>();
	}

	void Update()
	{
		if (ItemConsumed == true)
		{
			AttPotionActivate();
		}
	}


	void OnTriggerEnter(Collider Player)
	{
		if (Player.gameObject.tag == "Player")
		{
            ItemPickedUp();
        }
	}

	public void AttPotionActivate()
	{

        Player.BuffAttCalled = true;
        Debug.Log("Attack Up");
		Player.BuffStatsAtt = AttValue;
	}

	public override void ActivateItem()
	{
		base.ActivateItem();
		AttPotionActivate();
	}

    public void ItemPickedUp()
    {
        ItemTaken.ItemStorage.Add(Potion);
        gameObject.SetActive(false);
    }

}
