﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorePanelScript : MonoBehaviour 
{
	public HealthPotionScript HPPotion;
	public ManaPotion MPPotion;
	public AttackPotionScript AttPotion;
	public DefensePotionScript DefPotion;
	public InventoryScript Player;


	public void HP()
	{
		if(Player.TotalGold >= HPPotion.Value)
		{
			HealthPotionScript Potion = Instantiate (HPPotion);
            Potion.ItemPickedUp();
            Debug.Log ("HP Potion Added");
			Player.TotalGold -= HPPotion.Value;

		}
	}

	public void MP()
	{
		if (Player.TotalGold >= MPPotion.Value) 
		{
			ManaPotion Potion = Instantiate (MPPotion);
            Potion.ItemPickedUp();
            Debug.Log ("MP Potion Added");
			Player.TotalGold -= MPPotion.Value;
		}
	}

	public void Att()
	{
		if(Player.TotalGold >= AttPotion.Value)
		{
			AttackPotionScript Potion = Instantiate (AttPotion);
            Potion.ItemPickedUp();
			Debug.Log ("Att Potion Added");
			Player.TotalGold -= AttPotion.Value;
		}
	}

	public void Def()
	{
		if(Player.TotalGold >= DefPotion.Value)
		{
			DefensePotionScript Potion = Instantiate (DefPotion);
            Potion.ItemPickedUp();
            Debug.Log ("Def Potion Added");
			Player.TotalGold -= DefPotion.Value;
		}
	}
}
