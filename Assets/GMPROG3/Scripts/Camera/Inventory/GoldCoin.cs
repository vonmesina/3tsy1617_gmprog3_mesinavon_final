﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldCoin : MonoBehaviour 
{

	public int Value = 10;

	public InventoryScript ItemTaken;

	void Awake()
	{
		ItemTaken = GameObject.FindObjectOfType<InventoryScript> ();
	}

	void OnTriggerEnter(Collider Player)
	{
		if(Player.gameObject.tag == "Player")
		{
			gameObject.SetActive (false);
			ActivateItem ();
		}
	}

	public virtual void ActivateItem()
	{
		ItemTaken.TotalGold += Value;
		//Debug.Log("Plus 10 Gold");
		Destroy (gameObject);
	}
}
