﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaPotion : ItemScript 

{

	public InventoryScript ItemTaken;
	public ManaPotion Potion;
	public int ManaValue = 30;
	public PlayerStatsScript Player;

	void Awake()
	{
		ItemTaken = GameObject.FindObjectOfType<InventoryScript>();
		Player = GameObject.FindObjectOfType<PlayerStatsScript>();
	}

	void Update()
	{
		if (ItemConsumed == true)
		{
			ManaPotionActivate();
		}
	}


	void OnTriggerEnter(Collider Player)
	{
		if (Player.gameObject.tag == "Player")
		{
		    ItemPickedUp();
		}
	}

	public void ManaPotionActivate()
	{
		Debug.Log("Mana Healed");
		Player.Mana += ManaValue;
	}

	public override void ActivateItem()
	{
		base.ActivateItem();
		ManaPotionActivate();
	}

    public void ItemPickedUp()
    {
        ItemTaken.ItemStorage.Add(Potion);
        gameObject.SetActive(false);
    }

}
