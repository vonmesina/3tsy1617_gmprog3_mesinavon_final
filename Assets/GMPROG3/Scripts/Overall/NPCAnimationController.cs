﻿using UnityEngine;
using System.Collections;

public class NPCAnimationController : MonoBehaviour {

	Animator animator;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
	}

	// Update is called once per frame
	void Update () {

	}


	public void NPCAnim(int index)
	{
		animator.SetInteger ("AnimationIndex", index);
		Debug.Log (index);
	}

}
