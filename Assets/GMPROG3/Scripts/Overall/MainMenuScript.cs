﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuScript : MonoBehaviour 
{

	public void PlayButton()
	{
		Application.LoadLevel (2);
	}

	public void AnimButton()
	{
		Application.LoadLevel (1);
	}

	public void QuitButton()
	{
		Application.Quit ();
	}


}
