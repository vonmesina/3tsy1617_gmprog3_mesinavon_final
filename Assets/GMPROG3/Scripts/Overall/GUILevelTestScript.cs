﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUILevelTestScript : MonoBehaviour
{
	public Text
	Vitality,
	Strength,
	StatPoints,
	Health,
	Level,
	AttackDamage;

	public PlayerStatsScript Stats;

	void OnEnable()
	{
		UpdateInfo();
	}


	public void UpdateInfo()
	{
		Vitality.text = Stats.Vitality.ToString();
		Strength.text = Stats.Strength.ToString();
		StatPoints.text = Stats.LevelPoints.ToString();
		Level.text = Stats.Level.ToString ();
		Health.text = Stats.FullHealth.ToString();
		AttackDamage.text = Stats.AttackDamage.ToString ();
		//Stats.UpStats();
	}

	public void AddVitality()
	{
		if (Stats.LevelPoints > 0)
		{
			Stats.Vitality++;
			Stats.LevelPoints--;
			UpdateInfo();
		}
	}

	public void AddStrength()
	{
		if (Stats.LevelPoints > 0)
		{
			Stats.Strength++;
			Stats.LevelPoints--;
			UpdateInfo();
		}
	}

	public void AddLevel()
	{
		if (Stats.Level > 0)
		{
			//Stats.GainExperience (100);
			UpdateInfo();
			Debug.Log ("Leveled Up");
		}
		else if (Stats.Level <= 0)
		{
			Stats.Level = 1;
		}
	}
}