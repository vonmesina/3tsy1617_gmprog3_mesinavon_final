﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverallStats : MonoBehaviour {

	//Base Stats
	public int Vitality = 10, Strength = 2;
	//Death Counts
	private float DeathCount = 3;


	public virtual void Death()
	{
        //Debug.Log("DEATH");
		Destroy (gameObject,DeathCount);

	}

}
