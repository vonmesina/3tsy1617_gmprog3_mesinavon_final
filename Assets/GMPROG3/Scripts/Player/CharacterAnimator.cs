﻿using UnityEngine;
using System.Collections;

public class CharacterAnimator : MonoBehaviour {

	public CharacterScript Player;
	Animator animator;

	// Use this for initialization
	void Start () {
	
		Player = GetComponent<CharacterScript> ();
		animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (Player.State == PlayerState.Idle)
		{
			animator.SetInteger ("AnimationIndex", 0);
		}
	
		else if (Player.State == PlayerState.Run)
		{
			animator.SetInteger ("AnimationIndex", 1);
		}

		else if (Player.State == PlayerState.MovingTowards)
		{
			animator.SetInteger ("AnimationIndex", 2);
		}

		else if (Player.State == PlayerState.Attack)
		{
			animator.SetInteger ("AnimationIndex",3);
		}

		else if (Player.State == PlayerState.Attacked)
		{
			animator.SetInteger ("AnimationIndex",9);
		}

		else if (Player.State == PlayerState.Dead)
		{
			animator.SetInteger ("AnimationIndex",8);
		}

		else if (Player.State == PlayerState.SkillOne)
		{
			animator.SetInteger ("AnimationIndex",7);
		}
			
	}
}
