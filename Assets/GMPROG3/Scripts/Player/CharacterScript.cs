﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum PlayerState 
{

	Idle = 1,
	Run = 2,
	Attack = 3,
	MovingTowards = 4,
	Attacked = 5,
	Dead = 6,
	SkillOne = 7
};

public class CharacterScript : MonoBehaviour {


	public GameObject MarkerSpawn;
	public bool MarkerBool;
	public UnityEngine.AI.NavMeshAgent NavMeshX;
	public PlayerState State;
	public PlayerStatsScript Player;
	public float 
		DestinationPos = 0.5f, 
		EnemyDis = 1.0f;
	public MonsterStatsScript Enemy;
	public MonsterAIScript Target;
	public InventoryPanelScript InventoryActivate;
	public QuestPanelScript QuestActivate;
	private float 
		MarkerX,
		MarkerY,
		MarkerZ;
	private GameObject Marker;
	public GameObject
		Skill1,
		Skill2;
	private List<GameObject> Markers = new List<GameObject> ();

	IEnumerator WaitTimeSkillOne(float duration)
	{
		yield return new WaitForSeconds (duration);
		Skill1.gameObject.SetActive (true);
		yield return new WaitForSeconds (duration);
		Skill1.gameObject.SetActive (false);

	}
	IEnumerator WaitTimeSkillTwo(float duration)
	{
		Skill2.gameObject.SetActive (true);
		yield return new WaitForSeconds (duration);
		Skill2.gameObject.SetActive (false);

	}
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.I)) 
		{
			InventoryActivate.ToggleActive ();
		} 

		if(Input.GetKeyDown(KeyCode.Q))
		{
			QuestActivate.ToggleActive ();
		}

		if(!EventSystem.current.IsPointerOverGameObject())
		{
			CharacterMovement ();
		}
	}

	public void CharacterMovement()
	{
		if (Input.GetMouseButtonDown(0))
		{
			CancelInvoke("Attack");
			RaycastHit Hit;
			Ray Ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			//Gets the location of mouse click
			if (Physics.Raycast (Ray, out Hit, 100)) 
			{
				if (Player.Health > 0) 
				{
					if (Hit.transform.tag == "Terrain" || Hit.transform.tag == "Items" || Hit.transform.tag == "NPC") 
					{
						MarkerX = Hit.point.x;
						MarkerZ = Hit.point.z;
						MarkerY = Hit.point.y;
						if(Markers.Count <= 0)
						{
							MarkerSpawning ();
						}
						else if(Markers.Count > 0)
						{
							Markers.Remove (Marker);
							Destroy(Marker);
							MarkerSpawning ();
						}

						NavMeshX.Resume ();
						NavMeshX.SetDestination (Hit.point);
						State = PlayerState.Run;
                        if (Target != null)
                        {
                            Target.EnemyTargeted = false;
                        }
                        Enemy = null;
                        Target = null;
                        //Debug.Log ("Terrain Clicked");
                    } 
					else if (Hit.transform.gameObject.GetComponent<MonsterStatsScript> ()) 
					{
						Destroy(Marker);
						Enemy = Hit.transform.gameObject.GetComponent<MonsterStatsScript> ();
						Target = Hit.transform.gameObject.GetComponent<MonsterAIScript> ();

						Target.EnemyTargeted = true;
						State = PlayerState.MovingTowards;
						//Debug.Log ("Enemy Detected!");
					}
				}
			}

		}
		//Walk System
		if (State == PlayerState.Run)
		{
			if (DestinationPos >= Vector3.Distance(transform.position,NavMeshX.destination))
			{
				State = PlayerState.Idle;
				NavMeshX.Stop();

				Markers.Remove (Marker);
				Destroy(Marker);
				//Debug.Log ("Arrived");
			}
		}
		//Follow System
		else if (State == PlayerState.MovingTowards)
		{
			if (Vector3.Distance(transform.position, Enemy.transform.position) <= EnemyDis )
			{
                    State = PlayerState.Attack;
			}
			else if (Vector3.Distance(transform.position, Enemy.transform.position) > EnemyDis)
			{
				NavMeshX.SetDestination(Enemy.transform.position);
				NavMeshX.Resume ();

			}
		}
		else if (State == PlayerState.SkillOne)
		{
			NavMeshX.Stop ();
			Invoke ("SkillOne", 1.0f);
			State = PlayerState.Idle;
			CancelInvoke ("SkillOne");

		}
        //Attack System
		else if(State == PlayerState.Attack)
		{
			NavMeshX.Stop ();

            if (Enemy.Health > 0)
            {
				transform.LookAt (Target.transform);
            }
            else if(Enemy.Health <= 0)
            {
                State = PlayerState.Idle;
            }

        }
        
		else if(Player.Health <= 0)
		{
			PlayerDeath ();
		}
	}

    void DamageTo()
    {
        Enemy.Health -= Player.AttackDamage;
    }

	public void SkillOne()
	{
		//Enemy.Damaged ();
		CancelInvoke("SkillOne");
		Debug.Log ("Skill One Activated");
	}

    //Death Function
	public void PlayerDeath()
	{
		State = PlayerState.Dead;
		NavMeshX.Stop ();
		//Player.Death ();
	}

    //Marker Function
	public void MarkerSpawning()
	{
		
		Marker = Instantiate (MarkerSpawn, new Vector3(MarkerX, MarkerY + 1, MarkerZ), Quaternion.identity);
		Markers.Add (Marker);
	}

    //Skill(1) Function
	public void SkillOneActivate()
	{
		Debug.Log ("Skill(1)Activated");
		State = PlayerState.SkillOne;
		StartCoroutine (WaitTimeSkillOne(1.5f));
	}
	//Skill(2) Dunction
	public void SkillTwoActivate()
	{
		Debug.Log ("Skill(2)Activated");
		Player.Health = Player.FullHealth;
		StartCoroutine (WaitTimeSkillTwo(2f));
	}
		
}