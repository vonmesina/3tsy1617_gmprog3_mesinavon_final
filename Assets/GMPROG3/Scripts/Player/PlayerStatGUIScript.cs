﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStatGUIScript : MonoBehaviour 
{
	public Text
		Level,
		HP,
		MP,
		Att,
		Vit,
		Str,
		EXP,
		LP;

	public PlayerStatsScript Player;

	void Update()
	{
		
		UpdateData ();
	}

	void UpdateData()
	{
		Level.text = Player.Level.ToString ();
		HP.text = Player.Health.ToString ();
		MP.text = Player.Mana.ToString ();
		Att.text = Player.AttackDamage.ToString ();
		Vit.text = Player.Vitality.ToString ();
		Str.text = Player.Strength.ToString ();
		EXP.text = Player.CurrentExp.ToString ();
		LP.text = Player.LevelPoints.ToString ();

	}

	public void ToggleActive()
	{
		if(gameObject.activeSelf)
		{
			gameObject.SetActive (false);
		}
		else if(!gameObject.activeSelf)
		{
			gameObject.SetActive (true);
		}
	}

	public void UpVit()
	{
		if(Player.LevelPoints > 0)
		{
			Player.Vitality++;
			Player.LevelPoints--;
			Player.UpdateData ();
		}
	}

	public void UpStr()
	{
		if(Player.LevelPoints > 0)
		{
			Player.Strength++;
			Player.LevelPoints--;
			Player.UpdateData ();
		}
	}

}
