﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerGUIScript : MonoBehaviour 
{
	public Text
		LevelMain,
		Gold;
	public Image
		EXPBar,
		ManaBar,
		HealthBar;

	public PlayerStatsScript Player;
	public InventoryScript Items;


	void Awake()
	{
		Gold.text = "0";
	}

	void Update () 
	{
		UpdateGUI ();
	}


	private void UpdateGUI ()
	{

		LevelMain.text = Player.Level.ToString ();
		Gold.text = Items.TotalGold.ToString ();
		HealthBar.fillAmount = (float)Player.Health / (float)Player.FullHealth;
	    EXPBar.fillAmount = (float)Player.CurrentExp / (float)Player.MaxExp;
	}
}
