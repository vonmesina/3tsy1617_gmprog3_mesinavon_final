﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatsScript : OverallStats {

    public int
    Health,
    FullHealth,
    Mana,
    FullMana,
    AttackDamage,
    CurrentExp,
    MaxExp,
    Level = 1,
    LevelPoints = 0,
    SkillOneDamage = 20,
    BuffStatsAtt,
    BuffStatsDef,
    BuffTime;

    public bool
        BuffAttCalled = false,
        BuffDefCalled = false;

    IEnumerator WaitTimeAttBuff(float duration)
    {
        yield return new WaitForSeconds(duration);
        BuffStatsAtt = 0;
        BuffAttCalled = false;

    }
    void Start () 
	{
		UpdateData ();
	}

    void Update()
    {
        AttackDmg();

        if (BuffAttCalled == true)
        {
            StartCoroutine(WaitTimeAttBuff(BuffTime));
        }

    }

	public void CheckLevel()
	{
		while (CurrentExp >= 100) 
		{
			LevelUp ();
		}
	}

	public void LevelUp()
	{
		CurrentExp -=100;
		LevelPoints += 5;
		Level += 1;
		Health = FullHealth;
	}

	public void GainExp(int exp)
	{
		CurrentExp += exp;
		CheckLevel();
	}

	public void UpdateData()
	{
        Health = Vitality * 10;
		Mana = 100;
		FullMana = Mana;
		FullHealth = Health;
		CurrentExp = 0;
		MaxExp = 100;
	}

    public void AttackDmg()
    {
        AttackDamage = 4 * Strength + BuffStatsAtt; // BuffAtt
    }
    

}
