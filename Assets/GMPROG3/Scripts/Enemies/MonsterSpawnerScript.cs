﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MonsterSpawnerScript : MonoBehaviour 
{
	
	//Monster Spawns
	public List<GameObject> MonsterSpawns = new List<GameObject>();

	//Monster Settings
	public int totalEnemy = 5;
	private int numEnemy = 0;
	private int spawnedEnemy = 0;

	//Spawn Settings
	public bool Spawn = true;
	public float SpawnTimer = 15.0f;
	public float SpawnRate = 15.0f;
	public float SpawnDis = 50.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{

		if(Spawn)
		{
			if(SpawnTimer > 0)
			{
				SpawnTimer -= Time.deltaTime;
			}
			else if(SpawnTimer <= 0)
			{
				SpawnTimer = SpawnRate;

				if(spawnedEnemy >= totalEnemy)
				{
					Spawn = false;
				}
				else
				{
					spawnEnemy ();
				}
			}
		}

	}

	void spawnEnemy()
	{
		
		float x = Random.Range (-SpawnDis,SpawnDis);
		float z = Random.Range (-SpawnDis,SpawnDis);

		GameObject Enemy = Instantiate(MonsterSpawns[Random.Range(0, MonsterSpawns.Count)],new Vector3(x,2,z), transform.rotation) as GameObject;
		Enemy.transform.parent = transform;
		spawnedEnemy++;
	}

}
