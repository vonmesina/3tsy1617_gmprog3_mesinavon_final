﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterStatsScript : OverallStats {

	//Derived Stats
	public int
        Rand,
        RandX,
		ExpGain,
		Health, 
		FullHealth,
		AttackDamage;
	public GameObject Self;
    public List<GameObject> Drops = new List<GameObject>();
	public List<GameObject> Drop = new List<GameObject> ();
    void Start () 
	{
		Health = 10 * Vitality;
		FullHealth = Health;
		AttackDamage = 2 * Strength;
        Rand = Random.Range(0, 100);
        if (Rand <= 50)
        {
            RandX = 0;
        }
        else if (Rand >= 51)
        {
            RandX = 1;
        }
	}

    public void SpawnDrop()
    {
		if(Drop.Count == 0)
		{
		GameObject Bounty = Instantiate(Drops[RandX], new Vector3(Self.transform.position.x, Self.transform.position.y + 0.5f, Self.transform.position.z), Quaternion.identity) as GameObject;
		Drop.Add (Bounty);
        Debug.Log(Rand);
		}

	}

}
