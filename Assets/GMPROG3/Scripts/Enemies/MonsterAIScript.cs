﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public enum EnemyState
{
	Idle = 1,
	Patrol = 2,
	Chase = 3,
	Attack = 4,
	Attacked = 5,
	Dead = 6
}


public class MonsterAIScript : MonoBehaviour
{

    Animator anim;
	public MonsterStatsScript Monster;
	public UnityEngine.AI.NavMeshAgent NavMeshX;
	public EnemyState State;
	public PlayerStatsScript Target;
    public QuestListScript Quest;
	private float
		AttackDistance = 1.5f,
		ChaseDistance = 5.0f,
		PatrolDistance = 5.0f;
	public Image HPBarUI; 
	private Image HPBar;
	public GameObject CanvasParent;
	public GameObject Marker;
	public int MarkerNumber;
	private bool EXPOnce = false;
    private bool QuestOnce = false;
	public bool EnemyTargeted = false;
    public bool
        GoblinX,
        SlimeX,
        PigX;

    void Awake()
	{
		Marker = this.gameObject.transform.GetChild (MarkerNumber).gameObject;
		State = EnemyState.Patrol;
		Target = GameObject.FindObjectOfType<PlayerStatsScript>();
        Quest = GameObject.FindObjectOfType<QuestListScript>();
		CanvasParent = GameObject.Find ("PlayerInfoPanel");
		HPBarSpawning ();
		//HPBar.transform.position = Camera.main.WorldToViewportPoint (Monster.transform.position);
	}

	// Update is called once per frame
	void Update ()
    {

		if (Monster.Health > 0)
		{
            MonsterMovement();
		}

		else if(Monster.Health <= 0)
		{
			MonsterDeath ();
        }

		if (EnemyTargeted) 
		{
			Marker.SetActive (true);
            if (HPBar != null)
            {
                HPBar.gameObject.SetActive(true);
            }
		}
		else if (!EnemyTargeted) 
		{
			Marker.SetActive (false);
            if (HPBar != null)
            {
                HPBar.gameObject.SetActive(false);
            }
		}

	}

    public void MonsterMovement()
        {
        if (State == EnemyState.Patrol)
        {
            if (NavMeshX.destination == null)
            {
                NavMeshX.SetDestination(transform.position + new Vector3(Random.Range(-PatrolDistance, PatrolDistance), 0, Random.Range(-PatrolDistance, PatrolDistance)));
            }

            if (ChaseDistance >= Vector3.Distance(transform.position, Target.transform.position))
            {
                State = EnemyState.Chase;
            }

            if (3 >= Vector3.Distance(transform.position, NavMeshX.destination) || NavMeshX.destination == Target.transform.position)
            {
                NavMeshX.SetDestination(transform.position + new Vector3(Random.Range(-PatrolDistance, PatrolDistance), 0, Random.Range(-PatrolDistance, PatrolDistance)));
            }
        }
        else if (State == EnemyState.Chase)
        {
            if (Vector3.Distance(transform.position, Target.transform.position) <= AttackDistance)
            {
                State = EnemyState.Attack;
            }

            else if (Vector3.Distance(transform.position, Target.transform.position) >= Vector3.Distance(transform.position, Target.transform.position))
            {
				
				if (ChaseDistance > Vector3.Distance (transform.position, Target.transform.position)) {
					NavMeshX.Resume ();
					NavMeshX.SetDestination (Target.transform.position);
				}
				else if (ChaseDistance < Vector3.Distance(transform.position, Target.transform.position))
				{
					State = EnemyState.Patrol;
				}
            }

            
        }

        else if (State == EnemyState.Attack)
        {
            if (Vector3.Distance(transform.position, Target.transform.position) <= AttackDistance)
            {
				NavMeshX.Stop();
				if(Target.Health > 0)
				{
				transform.LookAt (Target.transform);
				}
				else if (Target.Health <= 0)
				{
					Target = null;
					State = EnemyState.Patrol;
				}
            }

            else if (Vector3.Distance(transform.position, Target.transform.position) > AttackDistance)
            {
                State = EnemyState.Chase;
            }


        }
    }

    public void MonsterDeath()
    {
        State = EnemyState.Dead;
        Monster.Death();
        NavMeshX.Stop();
		if(!EXPOnce)
		{	
			Target.GainExp (Monster.ExpGain);
		}
		EXPOnce = true;
        if (!QuestOnce)
        {
            if (Quest.QuestList.Count != 0)
            {
                QuestAdd();
            }
            QuestOnce = true;
        }
        Monster.SpawnDrop();
       
    }

    void DamageTo()
    {
        Target.Health -= Monster.AttackDamage;
    }

    public void HPBarSpawning()
	{
		HPBar = Instantiate (HPBarUI) as Image;
		//HPBar.transform.parent = CanvasParent.transform;
		HPBar.transform.SetParent(CanvasParent.transform,false);
	}

    void QuestAdd()
    {
        Quest.MonsterKill();
        if (GoblinX)
        {
            Quest.GoblinKill();
        }
        else if (SlimeX)
        {
            Quest.SlimeKill();
        }
        else if (PigX)
        {
            Quest.PigKill();
        }
    }
   
}
