﻿using UnityEngine;
using System.Collections;

public class MonsterAnimScript : MonoBehaviour {
	
	Animator animator;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
	}

	// Update is called once per frame
	void Update () {

	}


	public void MonsterAnim(int index)
	{
		animator.SetInteger ("AnimationIndex", index);
		Debug.Log (index);
	}

}
