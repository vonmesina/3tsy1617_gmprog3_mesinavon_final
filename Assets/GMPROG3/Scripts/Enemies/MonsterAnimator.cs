﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MonsterAnimator : MonoBehaviour {

	public MonsterAIScript Target;
	Animator Anim;

	// Use this for initialization
	void Awake () {
		Target = GetComponent<MonsterAIScript> ();
		Anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
	
		if(Target.State == EnemyState.Idle)
		{
			Anim.SetInteger ("AnimationIndex", 0);
		}

		if(Target.State == EnemyState.Patrol)
		{
			Anim.SetInteger ("AnimationIndex", 1);
		}

		if(Target.State == EnemyState.Chase)
		{
			Anim.SetInteger ("AnimationIndex", 2);
		}

		if(Target.State == EnemyState.Attack)
		{
			Anim.SetInteger ("AnimationIndex", 3);
		}

		if(Target.State == EnemyState.Attacked)
		{
			Anim.SetInteger ("AnimationIndex", 5);
		}

		if(Target.State == EnemyState.Dead)
		{
			Anim.SetInteger ("AnimationIndex", 6);
		}
	}
}
