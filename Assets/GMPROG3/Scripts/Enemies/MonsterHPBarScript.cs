﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MonsterHPBarScript : MonoBehaviour 
{
	public MonsterAIScript target;
	public Image HealthBar;
	public MonsterStatsScript Monster;
    float offsetY = 50;

    void Awake()
	{
		target = GameObject.FindObjectOfType<MonsterAIScript>();
		Monster = GameObject.FindObjectOfType<MonsterStatsScript>();
	}

	void Update () 
	{
		Vector3 wantedPos = Camera.main.WorldToScreenPoint(target.transform.position);
		Vector3 temp = transform.position;
        temp = new Vector3(wantedPos.x, wantedPos.y + offsetY, wantedPos.z);
		transform.position = temp;
		UpdateGUI ();

		if(HealthBar.fillAmount <= 0)
		{
			Destroy (gameObject);
		}

	}

	private void UpdateGUI()
	{
		HealthBar.fillAmount = (float)Monster.Health / (float)Monster.FullHealth;
	}
}
