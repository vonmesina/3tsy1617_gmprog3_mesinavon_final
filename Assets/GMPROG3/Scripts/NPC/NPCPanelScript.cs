﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPCPanelScript : MonoBehaviour 
{

	public KillMonstersQuestScript Quest1, RegisteredKMQ;
    public KillGoblinQuestScript Quest2, RegisteredKGQ;
    public KillPigQuestScript Quest3, RegisteredKPQ;
    public KillSlimeQuestScript Quest4, RegisteredKSQ;
    public QuestListScript QList;
    public GameObject Player;
    public bool Quest1Taken = false, Q1Done = false,
        Quest2Taken = false, Q2Done = false,
        Quest3Taken = false, Q3Done = false,
        Quest4Taken = false, Q4Done = false;

    public Button Q1Bttn, Q1FB,
                  Q2Bttn, Q2FB,
                  Q3Bttn, Q3FB,
                  Q4Bttn, Q4FB;

    public List<Text> TextList = new List<Text>();

    public void ToggleActive(GameObject Target)
    {
        if (gameObject.activeSelf)
        {
            gameObject.SetActive(false);
        }
        else if (!gameObject.activeSelf)
        {
            gameObject.SetActive(true);
            Debug.Log("NPC Activated");
        }

        Player = Target;
    }


    public void Start()
    {
        QList = GameObject.FindObjectOfType<QuestListScript>();
		Reset ();
        TextList[0].text = Quest1.QuestName.ToString();
        TextList[1].text = Quest2.QuestName.ToString();
        TextList[2].text = Quest3.QuestName.ToString();
        TextList[3].text = Quest4.QuestName.ToString();
    }

    public void Update()
    {
        if (Quest1Taken)
        {
            Q1Bttn.gameObject.SetActive(false);
        }
        if (Quest2Taken)
        {
            Q2Bttn.gameObject.SetActive(false);
        }
        if (Quest3Taken)
        {
            Q3Bttn.gameObject.SetActive(false);
        }
        if (Quest4Taken)
        {
            Q4Bttn.gameObject.SetActive(false);
        }

        if(Q1Done)
        {
            Q1FB.gameObject.SetActive(true);
        }
        if (Q2Done)
        {
            Q2FB.gameObject.SetActive(true);
        }
        if (Q3Done)
        {
            Q3FB.gameObject.SetActive(true);
        }
        if (Q4Done)
        {
            Q4FB.gameObject.SetActive(true);
        }

    }
    
	public void Store()
	{
		transform.GetChild(1).gameObject.SetActive(true);
		transform.GetChild(2).gameObject.SetActive(false);
        transform.GetChild(3).gameObject.SetActive(false);
        transform.GetChild(4).gameObject.SetActive(false);
        transform.GetChild(5).gameObject.SetActive(false);
    }

    public void Quest()
    {
        transform.GetChild(1).gameObject.SetActive(false);
        transform.GetChild(2).gameObject.SetActive(true);
        transform.GetChild(3).gameObject.SetActive(false);
        transform.GetChild(4).gameObject.SetActive(false);
        transform.GetChild(5).gameObject.SetActive(false);
    }

	public void Reset ()
	{
		transform.GetChild(1).gameObject.SetActive(false);
        transform.GetChild(2).gameObject.SetActive(false);
        transform.GetChild(3).gameObject.SetActive(true);
        transform.GetChild(4).gameObject.SetActive(true);
        transform.GetChild(5).gameObject.SetActive(true);
    }

	public void QuestTake1()
	{
        if (!Quest1Taken)
        {
            KillMonstersQuestScript KM = Instantiate(Quest1);
            KM.QuestEnable(Player);
            Quest1Taken = true;
            RegisteredKMQ = KM;
        }


	}
    public void QuestTake2()
    {
        if(!Quest2Taken)
        {
            KillGoblinQuestScript KG = Instantiate(Quest2);
            KG.QuestEnable(Player);
            Quest2Taken = true;
            RegisteredKGQ = KG;
        }
    }
    public void QuestTake3()
    {
        if (!Quest3Taken)
        {
            KillPigQuestScript KP = Instantiate(Quest3);
            KP.QuestEnable(Player);
            Quest3Taken = true;
            RegisteredKPQ = KP;
        }
        
    }
    public void QuestTake4()
    {
        if(!Quest4Taken)
        {
            KillSlimeQuestScript KS = Instantiate(Quest4);
            KS.QuestEnable(Player);
            Quest4Taken = true;
            RegisteredKSQ = KS;
        }
        
    }

    public void Q1Reward()
    {
        Quest1Taken = false;
        Q1Done = false;
        Q1FB.gameObject.SetActive(false);
        Q1Bttn.gameObject.SetActive(true);
        QList.QuestDone(RegisteredKMQ);
        RegisteredKMQ = null;
    }
    public void Q2Reward()
    {
        Quest2Taken = false;
        Q2Done = false;
        Q2FB.gameObject.SetActive(false);
        Q2Bttn.gameObject.SetActive(true);
        QList.QuestDone(RegisteredKGQ);
        RegisteredKGQ = null;
    }
    public void Q3Reward()
    {
        Quest3Taken = false;
        Q3Done = false;
        Q3FB.gameObject.SetActive(false);
        Q3Bttn.gameObject.SetActive(true);
        QList.QuestDone(RegisteredKPQ);
        RegisteredKPQ = null;
    }
    public void Q4Reward()
    {
        Quest4Taken = false;
        Q4Done = false;
        Q4FB.gameObject.SetActive(false);
        Q4Bttn.gameObject.SetActive(true);
        QList.QuestDone(RegisteredKSQ);
        RegisteredKSQ = null;
    }
}
