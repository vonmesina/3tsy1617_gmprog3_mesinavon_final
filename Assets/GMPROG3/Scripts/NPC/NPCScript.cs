﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCScript : MonoBehaviour 
{

	public NPCPanelScript NPCActivate;

	void OnTriggerEnter(Collider Player)
	{
		if(Player.gameObject.tag == "Player")
		{
			NPCActivate.ToggleActive (Player.gameObject);
		}

		Debug.Log ("NPC Reached");
	}
}
