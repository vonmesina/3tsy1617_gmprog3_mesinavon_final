﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestPanelScript : MonoBehaviour 
{
	public QuestListScript Quest;
	public List<Text> QuestInfoList = new List<Text>();

	void Start()
	{
		QuestInfoList.Capacity = 4;
	}
	void Update()
	{
        Refresh();
	}

	public void ToggleActive()
	{
		if (gameObject.activeSelf) 
		{
			gameObject.SetActive (false);
		} 
		else if (!gameObject.activeSelf) 
		{
			gameObject.SetActive (true);
			Debug.Log ("Quest Activated");
		}
	}

    public void CollectQuest(int QuestInt)
    {
        Quest.QuestList[QuestInt].QuestAchieved();
    }
    float refreshTimer = 0;
    void Refresh()
    {
        if (gameObject.activeSelf)
        {
            refreshTimer += Time.deltaTime;
            if (refreshTimer >= 1)
            {
                for (int value = 0; value < QuestInfoList.Count; value++)
                {
                    QuestInfoList[value].text = " ";
                }

                for (int value = 0; value < Quest.QuestList.Count; value++)
                {
                    QuestInfoList[value].text = Quest.QuestList[value].QuestName.ToString();
                 }
                refreshTimer = 0;
            }
        }
    }

}
