﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestScript : MonoBehaviour 
{

	public string QuestName = "NullQuest";
    public int MonsterKilled = 0;
    public int MonsterCount = 0;
    public int GoblinKilled = 0;
    public int SlimeKilled = 0;
    public int PigKilled = 0;
    public int LevelReward = 0;
    public int GoldReward = 0;
    public PlayerStatsScript Player;
    public InventoryScript Inv;
    public NPCPanelScript NPCP;

    void Start()
    {
        Player = GameObject.FindObjectOfType<PlayerStatsScript>();
        Inv = GameObject.FindObjectOfType<InventoryScript>();
        NPCP = GameObject.FindObjectOfType<NPCPanelScript>();
    }

	public virtual void QuestAchieved()
	{
		Debug.Log ("Quest Done");
        Player.GainExp(LevelReward);
        Inv.TotalGold += GoldReward;
	}


}
