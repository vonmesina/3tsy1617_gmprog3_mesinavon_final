﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillGoblinQuestScript : QuestScript
{ 
    string QuestNameTemp;
    bool questDone = false;

    void Awake()
    {
        GoblinKilled = 0;
        QuestNameTemp = QuestName;
    }

    void Update()
    {
        if (GoblinKilled >= MonsterCount)
        {
            if (!questDone)
            {
                QuestName = "Quest Done! Go to Betty!";
                questDone = true;
                NPCP.Q2Done = true;
            }
        }
    }

    public void QuestEnable(GameObject Player)
    {
        Player.GetComponent<QuestListScript>().QuestList.Add(this);
        Debug.Log("Quest(2) Added");
    }

}