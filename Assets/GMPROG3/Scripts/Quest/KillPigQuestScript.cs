﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPigQuestScript : QuestScript
{
    string QuestNameTemp;
    bool questDone = false;
    void Awake()
    {
        PigKilled = 0;
        QuestNameTemp = QuestName;
    }

    void Update()
    {
        if (PigKilled >= MonsterCount)
        {
            QuestName = "Quest Done! Go to Betty!";
            questDone = true;
            NPCP.Q3Done = true;
        }
    }

    public void QuestEnable(GameObject Player)
    {
        Player.GetComponent<QuestListScript>().QuestList.Add(this);
        Debug.Log("Quest(3) Added");
    }

}