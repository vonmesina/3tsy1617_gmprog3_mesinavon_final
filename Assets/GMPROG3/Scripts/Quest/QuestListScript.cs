﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestListScript : MonoBehaviour 
{

	public List<QuestScript> QuestList = new List<QuestScript>();

    void Start()
	{
		QuestList.Capacity = 4;

	}
    void Update()
    {
    }

	public void QuestDone(QuestScript X)
	{
		X.QuestAchieved ();
		QuestList.Remove (X);
        Destroy(X);
	}

    public void MonsterKill()
    {
        for(int i =0; i < QuestList.Count; i++)
        {
            QuestList[i].MonsterKilled += 1;
        }
    }
    public void GoblinKill()
    {
        for (int i = 0; i < QuestList.Count; i++)
        {
            QuestList[i].GoblinKilled += 1;
        }
    }

    public void SlimeKill()
    {
        for (int i = 0; i < QuestList.Count; i++)
        {
            QuestList[i].SlimeKilled += 1;
        }
    }

    public void PigKill()
    {
        for (int i = 0; i < QuestList.Count; i++)
        {
            QuestList[i].PigKilled += 1;
        }
    }
}
