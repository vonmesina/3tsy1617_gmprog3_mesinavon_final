﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillSlimeQuestScript : QuestScript
{
    string QuestNameTemp;
    bool questDone = false;

    void Awake()
    {
        SlimeKilled = 0;
        QuestNameTemp = QuestName;
    }

    void Update()
    {
        if (SlimeKilled >= MonsterCount)
        {
            if (!questDone)
            {
                QuestName = "Quest Done! Go to Betty!";
                questDone = true;
                NPCP.Q4Done = true;
            }
        }
    }

    public void QuestEnable(GameObject Player)
    {
        Player.GetComponent<QuestListScript>().QuestList.Add(this);
        Debug.Log("Quest(4) Added");
    }

}