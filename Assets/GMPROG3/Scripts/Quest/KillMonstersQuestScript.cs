﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KillMonstersQuestScript : QuestScript 
{
    string QuestNameTemp;
    bool questDone = false;

	void Awake()
	{
        MonsterKilled = 0;
        QuestNameTemp = QuestName;
    }

    void Update()
    {
        if (MonsterKilled >= MonsterCount)
        {
            if(!questDone)
            {
                QuestName = "Quest Done! Go to Betty!";
                questDone = true;
                NPCP.Q1Done = true;
            }
            
        }
    }

	public void QuestEnable(GameObject Player)
	{
        Player.GetComponent<QuestListScript>().QuestList.Add(this);
		Debug.Log ("Quest(1) Added");
	}
    
}
